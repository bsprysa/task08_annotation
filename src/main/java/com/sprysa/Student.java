package com.sprysa;

import java.lang.reflect.Method;

public class Student {

  @StudentCustomAnnotation(name = "Bogdan", age = 22, directionOfStudy = "Information security", yearOfStudy = 5)
  public void getStudent() {
  }

  public static void main(String[] args) {
    try {
      Method m = Student.class.getMethod("getStudent"); //Use reflection
      StudentCustomAnnotation studentAnnotation = (StudentCustomAnnotation) m
          .getAnnotation(StudentCustomAnnotation.class);
      if (studentAnnotation != null) {
        System.out.println("Name: " + studentAnnotation.name());
        System.out.println("Age: " + studentAnnotation.age());
        System.out.println("Direction of study: " + studentAnnotation.directionOfStudy());
        System.out.println("Year of study: " + studentAnnotation.yearOfStudy());
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
