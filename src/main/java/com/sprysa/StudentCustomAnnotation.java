package com.sprysa;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@interface StudentCustomAnnotation {
  String name();
  int age();
  String directionOfStudy();
  int yearOfStudy();
}
